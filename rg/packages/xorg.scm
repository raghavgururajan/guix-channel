(define-module (rg packages xorg)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (gnu packages)
  #:use-module (gnu packages xorg))

(define-public libxft-custom
  (package
    (inherit libxft)
    (name "libxft-custom")
    (version "2.3.4")
    (source
     (origin
       (method url-fetch)
       (uri (string-append "mirror://xorg/individual/lib/libXft-"
                           version ".tar.bz2"))
       (sha256
        (base32 "1pdbr6gzfvixc791pjf42i9gg8wvfq6cpq6sdca04h4i42mxmpjp"))
       (patches
	;; Provides support for color emojis.
        (list
         (origin
           (method url-fetch)
           (uri (string-append "https://gitlab.freedesktop.org/xorg/lib/libxft"
                               "/-/commit/723092ece088559f1af299236305911f4ee4d450.patch"))
           (file-name "libxft-composite-string.patch")
           (sha256
            (base32 "189naky0shllnifydc6r9wk904rxzcmvrb49y1rh36fb545y67d0")))
         (origin
           (method url-fetch)
           (uri (string-append "https://gitlab.freedesktop.org/xorg/lib/libxft"
                               "/-/commit/e0fc4ce7e87ab9c4b47e5c8e693f070dfd0d2f7b.patch"))
           (file-name "libxft-composite-text.patch")
           (sha256
            (base32 "14x87ciwczyvr77v9y1h9jn4qb9zqfcnm59a6lfg0g5yj3bhib11")))
         (origin
           (method url-fetch)
           (uri (string-append "https://gitlab.freedesktop.org/xorg/lib/libxft"
                               "/-/commit/d385aa3e5320d18918413df0e8aef3a713a47e0b.patch"))
           (file-name "libxft-bgra-glyphs.patch")
           (sha256
            (base32 "0nfxl9xf9mddxyfpagc0l77sp40pv00xqch14jh4xg2kmfqb00ng")))))))))


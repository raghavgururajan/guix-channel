(define-module (rg packages suckless)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (gnu packages)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages suckless)
  #:use-module (gnu packages xorg)
  #:use-module (rg packages xorg))

(define-public st-custom
  (package
    (inherit st)
    (name "st-custom")
    (version (package-version st))
    (source
     (origin
       (method url-fetch)
       (uri (string-append "https://dl.suckless.org/st/st-"
                           version ".tar.gz"))
       (sha256
        (base32 "0js9z5kn8hmpxzfmb2g6zsy28zkpg88j3wih5wixc89b8x7ms8bb"))
       (patches
        (list
         ;; Enables scrolling using Shift+{PageUp,PageDown}.
         (origin
           (method url-fetch)
           (uri (string-append "https://st.suckless.org/patches/scrollback/"
                               "st-scrollback-20210507-4536f46.diff"))
           (file-name "st-scrollback.patch")
           (sha256
            (base32 "072icbmj7my4c134d5apqw7v9q88vcrp6v6gdzf3668dzpkz9n0r")))
         ;; Enables scrolling using Shift+MouseWheel.
         (origin
           (method url-fetch)
           (uri (string-append "https://st.suckless.org/patches/scrollback/"
                               "st-scrollback-mouse-20220127-2c5edf2.diff"))
           (file-name "st-scrollback-mouse.patch")
           (sha256
            (base32 "0xjg9gyd3ag68srhs7fsjs8yp8sp2srhmjq7699i207bpz6rpb26")))
         ;; Disables scrolling using mouse wheel, when in MODE_ALTSCREEN.
         (origin
           (method url-fetch)
           (uri (string-append "https://st.suckless.org/patches/scrollback/"
                               "st-scrollback-mouse-altscreen-20220127-2c5edf2.diff"))
           (file-name "st-scrollback-mouse-altscreen.patch")
           (sha256
            (base32 "078hk7k5i0vc2x4dyb65dxd5ykr32pz4f4j6h0f7pm8j7xl1fbwg")))))))
    (arguments
     (substitute-keyword-arguments (package-arguments st)
       ((#:phases phases)
        `(modify-phases ,phases
           (add-after 'unpack 'patch
             (lambda _
               ;; Rename the config file, so that the changes
               ;; are picked up by build script.
               (rename-file "config.def.h" "config.h")
               (substitute* "config.h"
                 ;; Change font from Liberation to Unifont.
                 (("Liberation Mono:pixelsize=12")
                  "Unifont:pixelsize=16"))))))))
    (inputs
     (list
      fontconfig
      freetype
      libx11
      libxft-custom))))


(define-module (rg services base)
  #:use-module (gnu services)
  #:use-module (gnu services base))

(define-public (auto-login-to-tty config tty user)
  (if (string=? tty (mingetty-configuration-tty config))
      (mingetty-configuration
       (inherit config)
       (auto-login user))
      config))


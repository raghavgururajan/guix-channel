# Guix Channel

Custom definitions for [Guix](https://guix.gnu.org).

## Usage

### Try-out

Clone this repository and load its modules using Guix's load-path.

``` shell
git clone https://git.sr.ht/~raghavgururajan/guix-channel

guix command --load-path=path-to-repository package-name
```

### Subscription

Add this channel to your list of guix channels located at `$HOME/.config/guix/channels.scm`.

``` scheme
(append
 (list
  (channel
   (name 'rg)
   (url "https://git.sr.ht/~raghavgururajan/guix-channel")
   (introduction
    (make-channel-introduction
     "b56a4dabe12bfb1eed80467f48d389b32137cb60"
     (openpgp-fingerprint
      "CD2D 5EAA A98C CB37 DA91  D6B0 5F58 1664 7F8B E551")))))
 %default-channels)
```

Run `guix pull` to sync the channel's modules with your store.

## License

GNU General Public License v3.0 or later. See `COPYING`.
